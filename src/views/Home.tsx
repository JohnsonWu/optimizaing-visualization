import { useContext, useEffect, useState } from "react";
import aStar from "../algorithms/aStar";
import Node from "./../components/Node/Node";
import { GridContext } from "./../state/context";
import { useInitialGrid } from "./../state/hooks";
import './../components/Node/Node.css'

const method = "Astar";

const Home = () => {
  const data = useContext(GridContext);
  const [path, setPath] = useState([]);
  const [visitedNodes, setVisitedNodes] = useState<any>([]);
  const { node_start_row, node_start_col, node_end_row, node_end_col } = data;

  // Initialize Grid with Method
  const grid = useInitialGrid(method);

  useEffect(() => {
    if (Object.entries(grid).length !== 0 && method === "Astar") {
      let pathInfo = aStar(
        grid[node_start_row][node_start_col], // start node
        grid[node_end_row][node_end_col] // end node
      );
      setPath(pathInfo.path);
      setVisitedNodes(pathInfo.visitedPath);
    }
  }, [grid, node_end_col, node_end_row, node_start_col, node_start_row]);

  // Grid with Node
  const gridWithNode = (
    <div>
      {grid.map((row, rowIndex) => {
        return (
          <div key={rowIndex} className="flex flex-row justify-center">
            {row.map((nodeInfo: any, colIndex: number) => {
              const { isStart, isEnd } = nodeInfo;
              return <Node key={colIndex} isStart={isStart} isEnd={isEnd} row={rowIndex} col={colIndex} />;
            })}
          </div>
        );
      })}
    </div>
  );

  const visualizeShortestPath = (shortestPathNode: string | any[]) => {
    for (let i = 0; i < shortestPathNode.length; i++) {
      setTimeout(() => {
        const node = shortestPathNode[i];
        document.getElementById(`node-${node.i}-${node.j}`)!.className = "node node-shortest-path";
      }, 10 * i);
    }
  };

  const visualizePath = () => {
    for (let i = 0; i <= visitedNodes.length; i++) {
      if (i === visitedNodes.length) {
        setTimeout(() => {
          visualizeShortestPath(path);
        }, 20 * i);
      } else {
        setTimeout(() => {
          const node = visitedNodes[i];
          document.getElementById(`node-${node.i}-${node.j}`)!.className = "node node-visited";
        }, 20 * i);
      }
    }
  };

  return (
    <>
      <div className="mt-5 flex justify-center">
        <button className="border-2 rounded-xl p-2 border-blue-400 shadow-lg focus:outline-none focus:ring" onClick={visualizePath}>
          Press button
        </button>
      </div>
      <div className="container mx-auto mt-10 p-5">{gridWithNode}</div>
    </>
  );
};

export default Home;
