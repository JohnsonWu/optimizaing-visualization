import './Node.css'

interface NodeProps {
  isStart: boolean;
  isEnd: boolean;
  row: number;
  col: number;
}

const Node = ({ isStart, isEnd, row, col}: NodeProps) => {
  const classes = isStart ? "bg-blue-500" : isEnd ? "bg-red-500" : "";
  
  return <div className={`node ${classes}`} id={`node-${row}-${col}`}></div>;
};

export default Node;
