const aStar = (startNode: object, endNode: object) => {
  const heuristic = (a: any, b: any) => {
    let distance = Math.abs(a.i - a.j) + Math.abs(b.i - b.j);
    return distance;
  };

  let openSet: any = [];
  let closedSet: any = [];
  let path: any = [];
  let visitedPath: any = [];

  openSet.push(startNode);
  while (openSet.length > 0) {
    let winner = 0;
    for (let i = 0; i < openSet.length; i++) {
      if (openSet[i].f < openSet[winner].f) {
        winner = i;
      }
    }

    const current = openSet[winner];
    visitedPath.push(current);

    if (current === endNode) {
      // Find the Path
      path = [];
      let temp = current;
      path.push(temp);
      while (temp.previous) {
        path.push(temp.previous);
        temp = temp.previous;
      }
      return { path, visitedPath };
    }

    openSet = openSet.filter((elt: any) => elt !== current);
    closedSet.push(current);

    let neighbors = current.neighbors;
    for (let i = 0; i < neighbors.length; i++) {
      let neighbor = neighbors[i];
      if (!closedSet.includes(neighbor)) {
        let tempG = current.g + 1;
        let newPath = false;

        if (openSet.includes(neighbor)) {
          if (tempG < neighbor.g) {
            neighbor.g = tempG;
            newPath = true;
          }
        } else {
          neighbor.g = tempG;
          newPath = true;
          openSet.push(neighbor);
        }
        if (newPath) {
          neighbor.h = heuristic(neighbor, endNode);
          neighbor.f = neighbor.g + neighbor.h;
          neighbor.previous = current;
        }
      }
    }
  }

  return {path};
};

export default aStar;
