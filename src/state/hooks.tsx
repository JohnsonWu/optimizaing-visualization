import { useCallback, useEffect, useState, useContext } from "react";
import { AstarSpot } from "./astar";
import { GridContext } from "./../state/context";

export const useInitialGrid = (method: string) => {
  const data = useContext(GridContext);
  const { row, col, node_start_row, node_start_col, node_end_row, node_end_col } = data;
  const [grid, setGrid] = useState<any[]>([]);

  // Create Spot
  const createSpot = useCallback(
    (grid: any[]) => {
      for (let i = 0; i < row; i++) {
        for (let j = 0; j < col; j++) {
          switch (method) {
            case "Astar":
              grid[i][j] = new (AstarSpot as any)(row, col, i, j, node_start_row, node_start_col, node_end_row, node_end_col);
              break;

            default:
              break;
          }
        }
      }
    },
    [col, method, node_end_col, node_end_row, node_start_col, node_start_row, row]
  );

  // Initialize Grids
  const initialGrid = useCallback(() => {
    const grid = new Array(row);

    
    for (let i = 0; i < row; i++) {
      grid[i] = new Array(col);
    }
    
    const addNeighbors = (grid: any[]) => {
      for (let i = 0; i < row; i++) {
        for (let j = 0; j < col; j++) {
          grid[i][j].addNeighbors(grid);
        }
      }
    };
    
    createSpot(grid);

    setGrid(grid);

    addNeighbors(grid);
    
  }, [row, createSpot, col]);

  useEffect(() => {
    initialGrid();
  }, [initialGrid]);


  return grid;
};