import React from "react";

interface IContextProps {
  row: number,
  col: number,
  node_start_row: number,
  node_start_col: number,
  node_end_row: number,
  node_end_col: number,
}

export const GridContext = React.createContext({} as IContextProps);
