// Spot Constructor
export function AstarSpot(this: any, row: number, col: number, i: number, j: number, node_start_row: number, node_start_col: number, node_end_row: number, node_end_col: number) {
  this.i = i;
  this.j = j;
  this.isStart = this.i === node_start_row && this.j === node_start_col;
  this.isEnd = this.i === node_end_row && this.j === node_end_col;
  this.g = 0;
  this.h = 0;
  this.f = 0;
  this.previous = undefined;
  this.neighbors = [];  

  this.addNeighbors = (grid: any[]) => {
    let i = this.i;
    let j = this.j;
    if (i > 0) this.neighbors.push(grid[i - 1][j]);
    if (i < row - 1) this.neighbors.push(grid[i + 1][j]);
    if (j > 0) this.neighbors.push(grid[i][j - 1]);
    if (j < col - 1) this.neighbors.push(grid[i][j + 1]);
  };

}
