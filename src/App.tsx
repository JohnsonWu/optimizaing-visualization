import Home from "./views/Home";
import { GridContext } from './state/context'

const initialState = {
  row: 5,
  col: 5,
  node_start_row: 0,
  node_start_col: 0,
  node_end_row: 1,
  node_end_col: 3,
};

function App() {
  return (
    <GridContext.Provider value={initialState}>
      <Home />
    </GridContext.Provider>
  );
}

export default App;
